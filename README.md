# CC3200 Setup

This file contains the following information:
- How to build the AWS SDK and the samples for the TI CC3200 launchpad.
- How to run the samples.

THIS IS PRE-RELEASE SOFTWARE. CONTENTS AND DIRECTORY STRUCTURE ARE SUBJECT TO CHANGE.

## Table of Contents
- [Prerequisites](#Prerequisites)
- [Build the sample applications](#Build-SAMPLE)
- [Obtain certificate files](#Obtain-CERTS)
- [Build the certificate flasher tool](#Build-TOOL)
- [Setting up Code Composer Studio before running the samples](#Setup-CCS)
- [Running the certificate flasher tool](#Run-TOOL)
- [Running a sample](#Run-SAMPLE)

## Prerequisites
Required hardware: [CC3200 Launchpad](http://www.ti.com/tool/cc3200-launchxl) with the latest firmware/service pack.

This procedure assumes you have already installed the AWS SDK on your development machine. These instructions refer to the folder that contains the AWS SDK on your local machine as `<AWS_INSTALL_DIR>`.

While not strictly required, we recommend that you install the following tools from TI in the same directory and that you use directory names without any whitespace. This documentation assumes that you install everything in a directory named `C:/ti`.

- Install [Code Composer Studio v6.1.1](http://www.ti.com/tool/ccstudio?keyMatch=code%20composer%20studio) or above

- Install [CC3200 SDK 1.1.0](http://www.ti.com/tool/cc3200sdk). Linux users can use a tool such as [Wine](https://www.winehq.org) to run the SDK installer.

- Install [TI-RTOS SDK for SimpleLink 2.14.01.20](http://downloads.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/tirtos/index.html) or above

- Install [the NS package](http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/ns/ns_1_10_00_03.zip)

## Build the sample applications
1. Edit the `products.mak` file in `<AWS_INSTALL_DIR>/` using your favorite text editor. The variables `XDC_INSTALL_DIR, TIRTOS_INSTALL_DIR, BIOS_INSTALL_DIR, UIA_INSTALL_DIR, CC3200SDK_INSTALL_DIR` and `NS_INSTALL_DIR` must point to the locations where you installed these products. The variable `TI_ARM_CODEGEN_INSTALL_DIR` should point to the installation location of the TI ARM compiler in your CCS installation. After modification, these variable definitions should look similar to the following if you are working in Windows. (Windows users: note the use of "/" in the path).

```
    XDC_INSTALL_DIR ?= C:/ti/xdctools_3_31_01_33_core
    TIRTOS_INSTALL_DIR ?= C:/ti/tirtos_simplelink_2_14_01_20
    BIOS_INSTALL_DIR   ?= $(TIRTOS_INSTALL_DIR)/products/bios_6_42_02_29
    UIA_INSTALL_DIR    ?= $(TIRTOS_INSTALL_DIR)/products/uia_2_00_02_39
    NS_INSTALL_DIR     ?= C:/ti/ns_1_10_00_03
    CC3200SDK_INSTALL_DIR ?= C:/ti/CC3200SDK_1.1.0/cc3200-sdk
    TI_ARM_CODEGEN_INSTALL_DIR ?= C:/ti/ccsv6/tools/compiler/ti-cgt-arm_5.2.5
```

2. If you are using Windows, open a Windows command prompt. If you are using Linux, open a terminal window.

Before building a specific application, complete the following steps (we are referring to the `subscribe_publish_sample` application here, but the same procedure applies to other samples as well):

1. Open the `aws_iot_config.h` file from the directory `<AWS_INSTALL_DIR>/sample_apps/subscribe_publish_sample/` in a text editor and update the value of the "AWS_IOT_MQTT_HOST" variable with the address to the AWS Icebreaker endpoint.

2. Update the value of the "AWS_IOT_MQTT_CLIENT_ID" and "AWS_IOT_MY_THING_NAME" as per the AWS Developer Guide

3. Verify the file names in the variables "AWS_IOT_ROOT_CA_FILENAME", "AWS_IOT_CERTIFICATE_FILENAME", and "AWS_IOT_PRIVATE_KEY_FILENAME" match the ones in `<AWS_INSTALL_DIR>/certs/platform_tirtos/cc3200/certflasher.c`. If you are using the default paths in certflasher.c, no change should be required.

```
    #define AWS_IOT_ROOT_CA_FILENAME       "/cert/cacert.der" ///< Root CA file name
    #define AWS_IOT_CERTIFICATE_FILENAME   "/cert/clientcert.der" ///< device signed certificate file name
    #define AWS_IOT_PRIVATE_KEY_FILENAME   "/cert/clientkey.der" ///< Device private key filename
```

4. From the directory `<AWS_INSTALL_DIR>/sample_apps/subscribe_publish_sample/platform_tirtos/cc3200/`, Open the file `main.c`. Search for "USER STEP" and update the current date-time macros.

5. Open the file `wificonfig.h`. Search for "USER STEP" and update the WIFI SSID and SECURITY_KEY macros.

6. TI ARM compiler builds the C runtime library during the first application build which requires ```gmake``` on the environment path. It is recommended ```gmake``` tool is installed on your machine and added to the path. Alternately, XDCtools product (```C:/ti/xdctools_3_31_01_33_core```) which also contains ```gmake``` can be added to the path. But note, it is recommended that the XDCtools is removed from the path after the libraries are built. This is to avoid conflicts when you use newer versions of XDCtools. For detailed information about building TI ARM C runtime library, please read the [Mklib wiki](http://processors.wiki.ti.com/index.php/Mklib).

On the command line, enter the following commands to build the application:

```
    cd <AWS_INSTALL_DIR>/sample_apps/subscribe_publish_sample/platform_tirtos/cc3200
    C:/ti/xdctools_3_31_01_33_core/gmake all
```

<a name="Obtain-CERTS"></a>
## Obtain certificate files
Certificate files used by the samples need to be obtained from AWS using this procedure:

1. Create a client certificate and private key, and attach a policy using the instructions in the section "Securing Communication Between a Thing and AWS Iot" in the Icebreaker Developer Guide.

2. Obtain the root CA certificate as specified in the section “Verify MQTT Subscribe and Publish” in the Icebreaker Developer Guide.

<a name="Build-TOOL"></a>
## Build the certificate flasher tool
Before building the tool, complete the following:

1. Open the file `certflasher.c` from the directory `<AWS_INSTALL_DIR>/certs/platform_tirtos/cc3200/`.

2. Search for "USER STEP" and update the CA root certificate string, the client certificate string, and the client (private) key string. These should be extracted from certificate (.pem) files, or from the JSON data if you used the command line CLI tool in the previous section (remember to remove the newline '\n' characters in the JSON data). A typical string would be of this format:

    ```
    const char ca_pem[] =
    "JQQGEwJVUzEQMA4GA1UECAwHTW9udGFuYTEQMA4GA1UEBwwHQm96ZW1hbjERMA8G"
    "A1UECgwIU2F3dG9vdGgxEzARBgNVBAsMCkNvbnN1bHRpbmcxGDAWBlNVBAMMD3d3"
    "dy53b2xmc3NsLmNvbTEfMB0GCSqGSIb3DQEJARYQaW5mb0B3b2xmc3NsLmNvbTAe"
    "Fw0xNTA1MDcxODIxMDFaFw0xOGAxMzExODIxMDFaMIGUMQswCQYDVQQGEwJVUzEQ"
    "MA4GA1UECAwHTW9udGFuYTEQMA4GA1UEBwwHQm96ZW1hbjERMA8GA1UECgwIU2F3"
    "dG9vdGgxEzARBgNVBAsMCkNvbnN1bHRpbmcxGDAWBgNVBAMMD3d3dy53b2xmc3Ns"
    "LmNvbTEfMB0GCSqGSIb3DQEJARYQaW5mb0B3b2xmy3NsLmNvbTCCASIwDQYJKoZI"
    "hvcNAQEBBQADggEPADCCAQoCggEBAL8Myi0Ush6EQlvNOB9K8k11EPG2NZ/fyn0D"
    "mNOs3gNm7irx2LB9bgdUCxCYIU2AyxIg58xP3kV9yXJ3MurKkLtpUhADL6jzlcXx";
    ```

On the command line, enter the following commands to build the application:
    ```
    cd <AWS_INSTALL_DIR>/certs/platform_tirtos/cc3200
    C:/ti/xdctools_3_31_01_33_core/gmake all
    ```

<a name="Setup-CCS"></a>
## Setting up Code Composer Studio before running the samples
1. Plug the CC3200 Launchpad into a USB port on your PC

2. Open a serial session to the appropriate COM port with the following settings:

    ```
    Baudrate:     9600
    Data bits:       8
    Stop bits:       1
    Parity:       None
    Flow Control: None
    ```

3. Open Code Composer Studio.

4. In Code Composer Studio, open the CCS Debug Perspective - Windows menu -> Open Perspective -> CCS Debug

5. Open the Target Configurations View - Windows menu -> Show View -> Target Configurations

6. Right-click on User Defined. Select New Target Configuration.

7. Use `CC3200.ccxml` as "File name". Hit Finish.

8. In the Basic window, select "Stellaris In-Circuit Debug Interface" as the "Connection", and check the box next to "CC3200" in "Board or Device". Hit Save.

9. Right-click "CC3200.ccxml" in the Target Configurations View. Hit Launch Selected Configuration.

10. Under the Debug View, right-click on "Stellaris In-Circuit Debug Interface_0/Cortex_M4_0". Select "Connect Target".

<a name="Run-TOOL"></a>
## Running the certificate flasher tool
All samples rely on a set of certificates from AWS. As a result, the certificates need to be stored once into flash memory prior to running the samples. To flash the certificates, simply run the flasher tool you have previously [built](#Build-TOOL) using this procedure:

1. Select Run menu -> Load -> Load Program..., and browse to the file `certflasher.out` in `<AWS_INSTALL_DIR>/certs/platform_tirtos/cc3200/`. Hit OK. This would load the program onto the board.

2. Run the application by pressing F8. The output in the CCS Console looks as follows:

    ```
    Flashing ca certificate file ...
    Flashing client certificate file ...
    Flashing client key file ...
    done.
    ```

3. Hit Alt-F8 (Suspend) to halt the CPU.

<a name="Run-SAMPLE"></a>
## Running a sample
1. Select Run menu -> Load -> Load Program..., and browse to the file `subscribe_publish_sample.out` in `<AWS_INSTALL_DIR>/sample_apps/subscribe_publish_sample/platform_tirtos/cc3200`. Hit OK. This would load the program onto the board. (The same procedure applies to other samples by substituting `subscribe_publish_sample`)

2. Run the application by pressing F8. Output would appear in your serial terminal session:

    ```
    CC3200 has connected to AP and acquired an IP address.
    IP Address: 192.168.1.7

    AWS IoT SDK Version 0.3.0-BETA

    Connecting...
    Subscribing...
    -->sleep
    Subscribe callback
    sdkTest/sub     hello from SDK : 0
    -->sleep
    Subscribe callback
    sdkTest/sub     hello from SDK : 1
    -->sleep
    ```


#AWS IoT Embedded-C SDK

##Overview

The AWS IoT device SDK for embedded C is a collection of C source files which can be used in embedded applications to securely connect to the [AWS IoT platform](http://docs.aws.amazon.com/iot/latest/developerguide/what-is-aws-iot.html). It includes transport clients **(MQTT)**, **TLS** implementations and examples for their use. It also supports AWS IoT specific features such as **Thing Shadow**. It is distributed in source form and intended to be built into customer firmware along with application code, other libraries and RTOS. For additional information about porting the Device SDK for embedded C onto additional platforms please refer to the [Porting Guide](https://github.com/aws/aws-iot-device-sdk-embedded-C/blob/master/PortingGuide.md).

##Features
The Device SDK simplifies access to the Pub/Sub functionality of the AWS IoT broker via MQTT and provide APIs to interact with Thing Shadows. The SDK has been tested to work with the AWS IoT platform to ensure best interoperability of a device with the AWS IoT platform.

###MQTT Connection
The Device SDK provides functionality to create and maintain a mutually authenticated TLS connection over which it runs MQTT. This connection is used for any further publish operations and allow for subscribing to MQTT topics which will call a configurable callback function when these topics are received.

###Thing Shadow
The Device SDK implements the specific protocol for Thing Shadows to retrieve, update and delete Thing Shadows adhering to the protocol that is implemented to ensure correct versioning and support for client tokens. It abstracts the necessary MQTT topic subscriptions by automatically subscribing to and unsubscribing from the reserved topics as needed for each API call. Inbound state change requests are automatically signalled via a configurable callback.

## Design Goals of this SDK
The embedded C SDK was specifically designed for resource constrained devices (running on micro-controllers and RTOS).

Primary aspects are:
 * Flexibility in picking and choosing functionalities (reduce memory footprint)
 * Static memory only (no malloc’s)
 * Configurable resource usage(JSON tokens, MQTT subscription handlers, etc…)
 * Portability across RTOS due to use of wrapper functionality
 
For more information on the Architecture of the SDK refer [here](http://aws-iot-device-sdk-embedded-c-docs.s3-website-us-east-1.amazonaws.com/index.html)

##How to get started ?
Ensure you understand the AWS IoT platform and create the necessary certificates and policies. For more information on the AWS IoT platform please visit the [AWS IoT developer guide](http://docs.aws.amazon.com/iot/latest/developerguide/iot-security-identity.html).

In order to quickly get started with the AWS IoT platform, we have ported the SDK for POSIX type Operating Systems like Ubuntu, OS X and RHEL. The porting of the SDK happens at the TLS layer, and for the MQTT protocol. The SDK is configured for two TLS libraries and can be built out of the box with *GCC* using *make utility*. The tarballs can be downloaded from the below links.

* [OpenSSL](https://s3.amazonaws.com/aws-iot-device-sdk-embedded-c/linux_mqtt_openssl-1.0.1.tar)
* [mbedTLS from ARM](https://s3.amazonaws.com/aws-iot-device-sdk-embedded-c/linux_mqtt_mbedtls-1.0.1.tar)

##Installation
This section explains the individual steps to retrieve the necessary files and be able to build your first application using the AWS IoT device SDK for embedded C.

Steps:

 * Create a directory to hold your application e.g. (/home/<user>/aws_iot/my_app)
 * Change directory to this new directory
 * Download the SDK to device and place in the newly created directory
 * Expand the tarball (tar -xf <tarball.tar>).  This will create 4 directories:
 	* `aws_iot_src` - the AWS IoT SDK source files
 	* `sample_apps` - the sample applications
 	* `aws_mqtt_embedded_client_lib` - the aws MQTT client derived from [Eclipse Paho](http://www.eclipse.org/paho/clients/c/embedded/) Embedded C client
 	* `certs` - TLS certificates directory
 * Change directory to sample_apps.  The following sample applications are included:
 	* `subscribe_publish_sample` - a simple pub/sub MQTT example
 	* `shadow_sample` - a simple device shadow example using a connected window example
 	* `shadow_sample_console_echo` - a sample to work with the AWS IoT Console interactive guide
 * For each sample:
 	* Explore the example.  It connects to AWS IoT platform using MQTT and demonstrates few actions that can be performed by the SDK
 	* Build the example using make.  (''make'')
 	* Place device identity cert and private key in locations referenced in the example (certs/).  Alternatively, you can run the sample application with the ''-c'' flag to point to a specific certificate directory.
 	* Download certificate authority CA file from [Symantec](https://www.symantec.com/content/en/us/enterprise/verisign/roots/VeriSign-Class%203-Public-Primary-Certification-Authority-G5.pem) and place in location referenced in the example (certs/). Ensure the names of the cert files are the same as in the `aws_iot_config.h` file
 	* Run sample application (./subscribe_publish_sample or ./shadow_sample).  The sample will print status messages to stdout.
 	* More information on the examples could be found in the sample source file
 	
Also, for a guided example on getting started with the Thing Shadow, visit the AWS IoT Console's [Interactive Guide](https://console.aws.amazon.com/iot).

##Porting to different platforms
As Embedded devices run on different Real Time Operating Systems and TCP/IP stacks, it is one of the important design goals for the Device SDK to keep it portable. Please refer to the [porting guide](https://github.com/aws/aws-iot-device-sdk-embedded-C/blob/master/PortingGuide.md) to get more information on how to make this SDK run on your devices (i.e. micro-controllers).

##Resources
[API Documentation](http://aws-iot-device-sdk-embedded-c-docs.s3-website-us-east-1.amazonaws.com/index.html)

[MQTT 3.1.1 Spec](http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/csprd02/mqtt-v3.1.1-csprd02.html)

##Support
If you have any technical questions about AWS IoT Device SDK, use the [AWS IoT forum](https://forums.aws.amazon.com/forum.jspa?forumID=210).
For any other questions on AWS IoT, contact [AWS Support](https://aws.amazon.com/contact-us/).

##Sample APIs
Connecting to the AWS IoT MQTT platform
``` rc = aws_iot_mqtt_connect(&connectParams);```

Subscribe to a topic

```
MQTTSubscribeParams subParams = MQTTSubscribeParamsDefault; 
subParams.mHandler = MQTTcallbackHandler;
subParams.qos = QOS_0;
subParams.pTopic = "sdkTest/sub";
rc = aws_iot_mqtt_subscribe(&subParams);
```


Update Thing Shadow from a device
``` 
rc = aws_iot_shadow_update(&mqttClient,
AWS_IOT_MY_THING_NAME, 
pJsonDocumentBuffer, 
ShadowUpdateStatusCallback,
pCallbackContext, 
TIMEOUT_4SEC, 
persistenSubscription);
```